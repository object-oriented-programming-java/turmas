package br.com.ldrson.turmas;

import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;
import br.com.ldrson.turmas.funcionalidades.turmas.exibir.ExibirTurmaController;
import br.com.ldrson.turmas.funcionalidades.turmas.menu.MenuDeTurmasController;

/**
 * Created by leanderson on 24/06/16.
 */
public class Main {

    public static void main(String args[]){
        TurmaDAO dao = new TurmaDAO();

        /*


        Turma turma = new Turma();
        turma.setCodigo("001");
        turma.setNome("Programaçao Orientada a Objetos");
        turma.setCurso("Engenharia de Software");
        turma.setAno(2016);

        dao.salvar(turma);

        turma = new Turma();
        turma.setCodigo("002");
        turma.setNome("Programaçao Orientada a Objetos");
        turma.setCurso("Sistemas de Informação");
        turma.setAno(2016);

        dao.salvar(turma);

        turma = new Turma();
        turma.setCodigo("003");
        turma.setNome("Introdução a Teoria de Computação");
        turma.setCurso("Sistemas de Informação");
        turma.setAno(2016);

        dao.salvar(turma);

        for(Turma t : dao.obterTodos()){
            System.out.println(t.getCodigo()+" "+t.getNome()+" "+t.getCurso()+" "+t.getAno());
        }
        */
        //IncluirNovaTurmaController controller = new IncluirNovaTurmaController();
        //controller.executar();

        MenuDeTurmasController controller = new MenuDeTurmasController();
        controller.executar();

    }

}
