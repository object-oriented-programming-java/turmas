package br.com.ldrson.turmas.arquivo;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by leanderson on 24/06/16.
 */
public class GerenciadorDeArquivo {

    public void inicializarArquivo(String nomeDoArquivo) {

        try{

            FileWriter fileWriter = new FileWriter(nomeDoArquivo, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("");
            bufferedWriter.close();
            fileWriter.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public ArrayList<String> lerArquivoPorLinha(String nomeDoArquivo){

        ArrayList<String> lista = new ArrayList<>();

        try{

            FileReader fileReader = new FileReader(nomeDoArquivo);
            Scanner scanner = new Scanner(fileReader);

            while (scanner.hasNext()) {

                lista.add(scanner.nextLine());

            }

        }catch (FileNotFoundException e){
            System.out.println("ERRO :: arquivo com nome \""+nomeDoArquivo+"\" não foi econtrado.");
        }catch (Exception e){
            e.printStackTrace();
        }

        return lista;
    }

    public void adicionarLinhaNoFinalDoArquivo(String nomeDoArquivo, String linha) {

        try{

            FileWriter fileWriter = new FileWriter(nomeDoArquivo, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            bufferedWriter.write(linha);
            bufferedWriter.newLine();

            bufferedWriter.close();
            fileWriter.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void escreverNoArquivo(String nomeDoArquivo, ArrayList<String> linhas) {

        try{

            FileWriter fileWriter = new FileWriter(nomeDoArquivo);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for(String linha : linhas) {

                bufferedWriter.write(linha);
                bufferedWriter.newLine();
            }

            bufferedWriter.close();
            fileWriter.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void atualizarLinha(String nomeDoArquivo, String novaLinha, int posicaoDaLinha){

        ArrayList<String> arquivo = lerArquivoPorLinha(nomeDoArquivo);

        arquivo.set(posicaoDaLinha,novaLinha);

        escreverNoArquivo(nomeDoArquivo,arquivo);


    }

    public void excluirLinha(String nomeDoArquivo, int posicaoDaLinha){

        ArrayList<String> arquivo = lerArquivoPorLinha(nomeDoArquivo);

        arquivo.remove(posicaoDaLinha);

        escreverNoArquivo(nomeDoArquivo,arquivo);


    }

}
