package br.com.ldrson.turmas.comuns.dominio;

/**
 * Created by leanderson on 24/06/16.
 */
public class Aluno {

    public String codigo;
    private String nome;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
