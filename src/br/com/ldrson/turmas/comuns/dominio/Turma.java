package br.com.ldrson.turmas.comuns.dominio;

/**
 * Created by leanderson on 24/06/16.
 */
public class Turma {

    public String codigo;
    private String nome;
    private String curso;
    private int ano;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }
}
