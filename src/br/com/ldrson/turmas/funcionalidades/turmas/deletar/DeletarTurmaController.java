package br.com.ldrson.turmas.funcionalidades.turmas.deletar;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;

import java.util.Scanner;

/**
 * Created by leanderson on 28/06/16.
 */
public class DeletarTurmaController {

    private TurmaDAO mDao;
    private DeletarTurmaView mView;
    private Scanner mScanner;

    public DeletarTurmaController(){
        mDao = new TurmaDAO();
        mView = new DeletarTurmaView();
        mScanner = new Scanner(System.in);
    }

    public void executar(){
        mView.solicitarCodigoDaTurmaQueDesejaDeletar();
        String codigo = mScanner.nextLine();

        Turma turma = mDao.obter(codigo);

        if(turma == null){
            mView.exibirMensagemTurmaComCodigoInfomrmadoNaoExiste(codigo);
            mView.exibirMensagemDeParaProsseguir();
            mScanner.nextLine();
        }else{
            mView.exibirTurma(turma);
            mView.exibirMensagemDeConfirmacaoDeDelecao();
            codigo = mScanner.nextLine();
            if(codigo.equalsIgnoreCase("S")){
                mDao.deletar(turma);
                mView.exibirMensagemDeExclucaoComSucesso();
            }
        }
        mView.exibirMensagemDeParaProsseguir();
        mScanner.nextLine();
    }

}
