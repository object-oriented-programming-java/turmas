package br.com.ldrson.turmas.funcionalidades.turmas.deletar;

import br.com.ldrson.turmas.comuns.dominio.Turma;

/**
 * Created by leanderson on 28/06/16.
 */
public class DeletarTurmaView {

    public void solicitarCodigoDaTurmaQueDesejaDeletar(){
        System.out.println("|================================================|");
        System.out.println("|                   TURMA                        |");
        System.out.println("|================================================|");
        System.out.println("|  Informe o código da turma que deseja deletar: ");
    }

    public void exibirTurma(Turma turma){
        System.out.println("|================================================|");
        System.out.println("|                   TURMA                        |");
        System.out.println("|================================================|");
        System.out.println("|                                                |");
        System.out.println("|  Código : "+turma.getCodigo());
        System.out.println("|  Nome   : "+turma.getNome());
        System.out.println("|  Curso  : "+turma.getCurso());
        System.out.println("|  Ano    : "+turma.getAno());
        System.out.println("|                                                |");
        System.out.println("|================================================|");

    }


    public void exibirMensagemTurmaComCodigoInfomrmadoNaoExiste(String codigo) {
        System.out.println("|================================================|");
        System.out.println("|  A turma com o código ["+codigo+"] não existe. ");
        System.out.println("|================================================|");
    }

    public void exibirMensagemDeConfirmacaoDeDelecao() {
        System.out.println("|  Para confirmar a exclusão da Turma digite \"S\" ");
        System.out.println("|================================================|");
    }

    public void exibirMensagemDeExclucaoComSucesso() {
        System.out.println("|  Turma deletado com sucesso.                   |");
        System.out.println("|================================================|");
    }

    public void exibirMensagemDeParaProsseguir() {
        System.out.println("|  Tecle ENTER para continuar                    |");
        System.out.println("|================================================|");
    }

}
