package br.com.ldrson.turmas.funcionalidades.turmas.editar;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;

import java.util.Scanner;

/**
 * Created by leanderson on 28/06/16.
 */
public class EditarTurmaController {

    private TurmaDAO mDao;
    private EditarTurmaView mView;
    private Scanner mScanner;

    public EditarTurmaController(){
        mDao = new TurmaDAO();
        mView = new EditarTurmaView();
        mScanner = new Scanner(System.in);
    }

    public void executar(){

        mView.solicitarCodigoDaTurmaQueDesejaEditar();
        String codigo = mScanner.nextLine();

        Turma turma = mDao.obter(codigo);

        if(turma == null){
            turmaNaoExiste(codigo);
        }else{
            mView.exibirTurma(turma);
            mView.exibirMensagemDeConfirmacaoDeEdicao();
            String resposta = mScanner.nextLine();
            if(resposta.equalsIgnoreCase("S")){
                editarTurma(codigo);
            }
        }
        mView.exibirMensagemDeParaProsseguir();
        mScanner.nextLine();
    }

    private void turmaNaoExiste(String codigo){
        mView.exibirMensagemTurmaComCodigoInfomrmadoNaoExiste(codigo);
        mView.exibirMensagemDeParaProsseguir();
        mScanner.nextLine();
    }

    private void editarTurma(String codigo){
        Turma turma = new Turma();
        turma.setCodigo(codigo);

        mView.exibirSolicitacaoDoNome();
        turma.setNome(mScanner.nextLine());

        mView.exibirSolicitacaoDoCurso();
        turma.setCurso(mScanner.nextLine());

        mView.exibirSolicitacaoDoAno();
        turma.setAno(mScanner.nextInt());
        mScanner.nextLine();

        mDao.salvar(turma);
        mView.exibirMensagemDeExclucaoComSucesso();
    }

}
