package br.com.ldrson.turmas.funcionalidades.turmas.incluir;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;

import java.util.Scanner;

/**
 * Created by leanderson on 24/06/16.
 */
public class IncluirNovaTurmaController {

    private IncluirNovaTurmaView mView;
    private Scanner mScanner;
    private TurmaDAO mDao;

    public IncluirNovaTurmaController(){
        mView = new IncluirNovaTurmaView();
        mScanner = new Scanner(System.in);
        mDao = new TurmaDAO();
    }

    public void executar(){

        Turma turma = new Turma();

        mView.exibirCabecalho();

        mView.exibirSolicitacaoDoCodigo();
        turma.setCodigo(mScanner.nextLine());

        mView.exibirSolicitacaoDoNome();
        turma.setNome(mScanner.nextLine());

        mView.exibirSolicitacaoDoCurso();
        turma.setCurso(mScanner.nextLine());

        mView.exibirSolicitacaoDoAno();
        turma.setAno(mScanner.nextInt());
        mScanner.nextLine();

        mDao.salvar(turma);

        mView.exibirMensagemDeTurmaCadastradaComSucesso();
        mView.exibirMensagemDeParaProsseguir();

        mScanner.nextLine();

    }

}
