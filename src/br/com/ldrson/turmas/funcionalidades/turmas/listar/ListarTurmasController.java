package br.com.ldrson.turmas.funcionalidades.turmas.listar;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by leanderson on 24/06/16.
 */
public class ListarTurmasController {

    private ListarTurmasView mView;
    private TurmaDAO mDao;
    private Scanner mScanner;

    public ListarTurmasController(){
        mView = new ListarTurmasView();
        mScanner = new Scanner(System.in);
    }

    public void executar(){
        mDao = new TurmaDAO();
        ArrayList<Turma> turmas = mDao.obterTodos();

        if(turmas.isEmpty()){
            mView.exibirMensagemNaoHaTurmasCadastradas();
        }else{
            mView.exibirTurmas(turmas);
        }
        mView.exibirMensagemDeParaProsseguir();
        mScanner.nextLine();

    }


}
