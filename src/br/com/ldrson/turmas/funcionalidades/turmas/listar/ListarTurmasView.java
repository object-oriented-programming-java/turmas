package br.com.ldrson.turmas.funcionalidades.turmas.listar;

import br.com.ldrson.turmas.comuns.dominio.Turma;

import java.util.ArrayList;

/**
 * Created by leanderson on 24/06/16.
 */
public class ListarTurmasView {

    public void exibirTurmas(ArrayList<Turma> turmas){
        System.out.println("|================================================|");
        System.out.println("|              LISTA DE TURMAS                   |");
        System.out.println("|================================================|");
        System.out.println("|                                                |");
        System.out.println("| Código\t\t| Nome\t\t|Curso\t\t|Ano\t");
        System.out.println("|                                                |");
        for(Turma t : turmas){
            System.out.println("| "+t.getCodigo()+"\t| "+t.getNome()+"\t| "+t.getCurso()+"\t| "+t.getAno());
            System.out.println("");
        }
    }

    public void exibirMensagemNaoHaTurmasCadastradas() {
        System.out.println("|  Não há turmas cadastradas                     |");
        System.out.println("|================================================|");
    }

    public void exibirMensagemDeParaProsseguir() {
        System.out.println("|  Tecle ENTER para continuar                    |");
        System.out.println("|================================================|");
    }

}
