package br.com.ldrson.turmas.funcionalidades.turmas.menu;

import br.com.ldrson.turmas.funcionalidades.turmas.deletar.DeletarTurmaController;
import br.com.ldrson.turmas.funcionalidades.turmas.editar.EditarTurmaController;
import br.com.ldrson.turmas.funcionalidades.turmas.exibir.ExibirTurmaController;
import br.com.ldrson.turmas.funcionalidades.turmas.incluir.IncluirNovaTurmaController;
import br.com.ldrson.turmas.funcionalidades.turmas.listar.ListarTurmasController;
import br.com.ldrson.turmas.funcionalidades.turmas.pesquisar.PesquisarTurmaController;

import java.util.Scanner;

/**
 * Created by leanderson on 24/06/16.
 */
public class MenuDeTurmasController {

    private MenuDeTurmasView mView;
    private Scanner mScanner;

    public MenuDeTurmasController(){
        mView = new MenuDeTurmasView();
        mScanner = new Scanner(System.in);
    }

    public void executar(){

        boolean sair = false;

        while(!sair){
            mView.exibirOpcoes();

            String opcaoDigitada = mScanner.nextLine();

            if(opcaoFoiSelecionada(opcaoDigitada,"L")){
                ListarTurmasController listarTurmasController = new ListarTurmasController();
                listarTurmasController.executar();
            }else if(opcaoFoiSelecionada(opcaoDigitada,"I")){
                IncluirNovaTurmaController incluirNovaTurmaController = new IncluirNovaTurmaController();
                incluirNovaTurmaController.executar();
            }else if(opcaoFoiSelecionada(opcaoDigitada,"C")){
                ExibirTurmaController exibirTurmaController = new ExibirTurmaController();
                exibirTurmaController.executar();
            }else if(opcaoFoiSelecionada(opcaoDigitada,"E")){
                EditarTurmaController controller = new EditarTurmaController();
                controller.executar();
            }else if(opcaoFoiSelecionada(opcaoDigitada,"D")){
                DeletarTurmaController deletarTurmaController = new DeletarTurmaController();
                deletarTurmaController.executar();
            }else if(opcaoFoiSelecionada(opcaoDigitada,"X")){
                sair = true;
            }else if(opcaoFoiSelecionada(opcaoDigitada,"P")){
                PesquisarTurmaController pesquisarTurmaController = new PesquisarTurmaController();
                pesquisarTurmaController.executar();
            }

        }

    }

    private boolean opcaoFoiSelecionada(String opcaoDigitada, String codigoDaOpcao){
        return opcaoDigitada != null && opcaoDigitada.equalsIgnoreCase(codigoDaOpcao);
    }

}
