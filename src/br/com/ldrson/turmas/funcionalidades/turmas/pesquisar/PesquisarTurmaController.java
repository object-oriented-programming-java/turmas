package br.com.ldrson.turmas.funcionalidades.turmas.pesquisar;

import br.com.ldrson.turmas.comuns.dominio.Turma;
import br.com.ldrson.turmas.funcionalidades.turmas.comuns.TurmaDAO;
import br.com.ldrson.turmas.funcionalidades.turmas.listar.ListarTurmasView;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by leanderson on 28/06/16.
 */
public class PesquisarTurmaController {

    private PesquisarTurmaView mView;
    private TurmaDAO mDao;
    private Scanner mScanner;

    public PesquisarTurmaController(){
        mView = new PesquisarTurmaView();
        mScanner = new Scanner(System.in);
        mDao = new TurmaDAO();
    }

    public void executar(){
        mView.solicitarNomeDaTurmaQueDesejaPesquisar();
        String termoDePesquisa = mScanner.nextLine();
        ArrayList<Turma> turmas = mDao.obterTodosComONome(termoDePesquisa);
        if(turmas.isEmpty()){
            mView.exibirMensagemNaoFoiLocalizadaTurmasComONome(termoDePesquisa);
        }else{
            mView.exibirTurmas(termoDePesquisa,turmas);
        }
        mView.exibirMensagemDeParaProsseguir();
        mScanner.nextLine();
    }

}
