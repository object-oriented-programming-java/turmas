package br.com.ldrson.turmas.funcionalidades.turmas.pesquisar;

import br.com.ldrson.turmas.comuns.dominio.Turma;

import java.util.ArrayList;

/**
 * Created by leanderson on 28/06/16.
 */
public class PesquisarTurmaView {

    public void solicitarNomeDaTurmaQueDesejaPesquisar(){
        System.out.println("|================================================|");
        System.out.println("|                   TURMA                        |");
        System.out.println("|================================================|");
        System.out.println("|  Informe o nome da turma que deseja pesquisar: ");
    }


    public void exibirTurmas(String termoDePesquisa, ArrayList<Turma> turmas){
        System.out.println("|================================================|");
        System.out.println("|  Pesquisa de Turmas por nome : "+termoDePesquisa);
        System.out.println("|================================================|");
        System.out.println("|                                                |");
        System.out.println("| Código\t\t| Nome\t\t|Curso\t\t|Ano\t");
        System.out.println("|                                                |");
        for(Turma t : turmas){
            System.out.println("| "+t.getCodigo()+"\t| "+t.getNome()+"\t| "+t.getCurso()+"\t| "+t.getAno());
            System.out.println("");
        }
    }

    public void exibirMensagemNaoFoiLocalizadaTurmasComONome(String termoDePesquisa) {
        System.out.println("|  Não foram localizadas turmas com o nome : "+termoDePesquisa);
        System.out.println("|================================================|");
    }

    public void exibirMensagemDeParaProsseguir() {
        System.out.println("|  Tecle ENTER para continuar                    |");
        System.out.println("|================================================|");
    }

}
